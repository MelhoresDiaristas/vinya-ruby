module Vinya
  class Resource < ActiveResource::Base
    attr_accessor :response_body, :response, :response_code
    after_save :set_response
    def initialize(attributes = {}, persisted = false)
      set_attr_resource
      raise 'You need to configure a API key before performing requests.' unless Vinya.api_token
      super
      @attributes = @attributes.merge( (known_attributes - attributes.keys.map(&:to_s)).collect { |item| [item, ""] }.to_h)
      self
    end

    def set_attr_resource
      Vinya::Resource.site = Vinya.api_endpoint
      Vinya::Resource.headers["VinyaToken"] = Vinya.api_token
      Vinya::Resource.include_format_in_path = false
    end

    def save
      begin
        super
      rescue => e
        set_response
        raise e
      end
    end

    def set_response
      self.response_body = connection.body
      self.response = connection.response
      self.response_code = connection.code
    end
  end

end
