class ActiveResource::Connection
  # Creates new Net::HTTP instance for communication with
  # remote service and resources.
  attr_accessor :body, :code, :response
  #for debug
  #def http
  #  http = Net::HTTP.new(@site.host, @site.port)
  #  http.use_ssl = @site.is_a?(URI::HTTPS)
  #  http.verify_mode = OpenSSL::SSL::VERIFY_NONE if http.use_ssl?
  #  http.read_timeout = @timeout if @timeout
  #  # Here's the addition that allows you to see the output
  #  http.set_debug_output $stderr
  #  return http
  #end

  def request(method, path, *arguments)
    result = ActiveSupport::Notifications.instrument("request.active_resource") do |payload|
      payload[:method]      = method
      payload[:request_uri] = "#{site.scheme}://#{site.host}:#{site.port}#{path}"
      payload[:result]      = http.send(method, path, *arguments)
    end
    self.body = result.body
    self.code = result.code
    self.response = result.response
    handle_response(result)
  rescue Timeout::Error => e
    raise TimeoutError.new(e.message)
  rescue OpenSSL::SSL::SSLError => e
    raise SSLError.new(e.message)
  end
end
