API VINYA - AUTORIZAÇÃO E CAPTURA SIMULTÂNEAS

A API VINYA recebe um objeto de solicitação de Autorização e Captura que é processado junto
a Adquirente sincronamente e, internamente, de forma assíncrona.
O objeto de entrada possui a seguinte estrutura:
{
"Amount": 1000,
"CreditCardBrand": "visa",
"CreditCardNumber": "0000000000000001",
"Month": 10,
"Year": 2020,
"SecurityCode": "123",
"HolderName": "Holder name",
"Installment": 1,
"OrderId": "31152b33-e5d5-4528-ab9c-7d711d41a12a",
"CustomerId": "CUST_TEST01"
}
• Amount (int): Valor da transação em centavos, considerando duas casa decimais. Assim,
Amount: 1000 significa um valor de R$10,00.
• CreditCardBrand (string): Bandeira do cartão de crédito: visa / mastercard / amex / elo /
aura / jcb / diners / discover
• CreditCardNumber (string): Número do cartão de crédito.
• Month (int): Mês de vencimento do cartão de crédito.
• Year (int): Ano de vencimento do cartão de crédito.
• SecurityCode (string): Código de segurança do cartão de crédito.
• HolderName (string): Nome impresso no cartão de crédito.
• Installment (int): Número de parcelas.
• OrderId (string): Id da ordem de compra do seu sistema.
• CustomerId (string): Token do cliente
Além dessas informações, todas as requisições devem acompanhar um HTTP Header contendo
o VinyaToken, que é um token de autorização.

Como retorno, devolvemos uma resposta com a seguinte estrutura:
{
"VinyaTransactionId": "0f35063e-c8bf-4def-a621-d306fed808a5",
"StatusCode": 0,
"StatusMessage": "",
"OrderId": "31152b33-e5d5-4528-ab9c-7d711d41a12a"
}

• VinyaTransactionId (string): Id da transação na Vinya.
• StatusCode (int): Código de retorno 0 (Zero) indica que a transação foi autorizada e
capturada. Caso haja falha na autorização e captura, o retorno será um http status 200 e o
StatusCode diferente de zero. Outros valores acompanharão uma mensagem.
• StatusMessage (string): Mensagem de retorno. Em caso de sucesso, esse campo vem com
uma string vazia. Em caso de falhas, uma mensagem com maiores detalhes será
retornada.
• OrderId (string): Id da ordem de compra no sistema solicitante.
O VinyaToken é a garantia de que a transação vem, efetivamente, do solicitante esperado. No
caso da ausência do VinyaToken ou na presença de um Token inválido, será retornado um http
status 400 e nenhum objeto de retorno.
Caso o id do cliente esteja incorreto, e o cliente esteja bloqueado ou a requisição esteja
incompleta, será retornado um http status 400 e o objeto descrito acima com o status code
diferente de zero.
Caso haja algum problema com a Captura e Autorização junto ao Adquirente, será retornado
um http status 200, status code diferente de zero e uma mensagem vinda do Adquirente.
Se tudo ocorrer corretamente, será retornado um http status 200, status code zero e nenhuma
status message.
Para realizar testes de Autorização e Captura Simultâneas na Sandbox Vinya, é indispensável
ter os seguintes dados:
Endpoint da sandbox: https://sandboxec1.vinya.com.br/api/GatewayPayment;
VinyaToken (exemplo: B10DEA11-31D4-4AB7-E0DE-CC3CA1D55A7D);
ContractToken (exemplo: 150CEFA6-C150-43A4-8A61-0E82F60F0EF0).

Para realização dos testes em sandbox, duas referências são pertinentes:
1) o retorno das transações pode ser planejado conforme o final do número do cartão.
Sugerimos que, ao invés de utilizar número de cartões reais, sejam utilizados números
inexistentes, apenas para efeito de testes. Se, no entanto, números de cartões reais
forem utilizados, o efeito será conforme o número final cartão, conforme a tabela
abaixo;
2) As informações de Cód.Segurança (CVV) e validade podem ser aleatórias, mantendo o
formato - CVV (3 dígitos) Validade (MM/YYYY).

STATUS DA TRANSAÇÃO FINAL DO CARTÃO
Autorizado 1
Autorizado 4
Não Autorizado 2
Não Autorizado 3
Não Autorizado 5
Não Autorizado 6
Não Autorizado 7
Não Autorizado 8
Autorização Aleatória 9

Para que o VinyaToken e o ContractToken para o ambiente de sandbox possam ser gerados e
fornecidos, é necessário que um cadastro válido seja efetuado no ambiente de sandbox da
Vinya. Para tanto, as informações pertinentes devem ser disponibilizadas para a Vinya. Caso
ainda não tenha sido realizado esse cadastramento, solicite à Vinya o formulário para
cadastramento.
Posteriormente, após os testes em sandbox e a homologação da solução, novas credenciais
(VinyaToken e ContractToken) deverão ser geradas para utilização no ambiente de produção.

API VINYA - CANCELAMENTO
A API VINYA recebe um objeto de solicitação de Cancelamento que é processado junto a
Adquirente sincronamente e, internamente, de forma assíncrona.
O objeto de entrada possui a seguinte estrutura:
{
"Amount": 1000,
"VinyaTransactionId": "31152b33-e5d5-4528-ab9c-7d711d41a12a",
"CustomerId": "31152b33-e5d5-4528-ab9c-7d711d41a12a",
}
Amount (int): Valor da transação em centavos, considerando duas casa decimais. Assim,
Amount: 1000 significa um valor de R$10,00.
VinyaTransactionId (string): Id da transação na Vinya.
CustomerId (string): Token do cliente.
Além dessas informações, todas as requisições devem acompanhar um HTTP Header contendo
o VinyaToken, que é um token de autorização.
O VinyaToken é a garantia de que a transação vem, efetivamente, do solicitante esperado. No
caso da ausência do VinyaToken ou na presença de um Token inválido, será retornado um http
status 400 e nenhum objeto de retorno.
Como retorno, devolvemos uma resposta com a seguinte estrutura:
{
"VinyaTransactionId": "0f35063e-c8bf-4def-a621-d306fed808a5",
"StatusCode": 0,
"StatusMessage": "",
"OrderId": "3b425f74-19d9-495a-80b2-176cfdee74e0"
}
VinyaTransactionId (string): Id da transação na Vinya.
StatusCode (int): Código de retorno 0 (Zero) indica que a transação foi autorizada e capturada.
Caso haja falha na autorização e captura, o retorno será um http status 200 e o StatusCode
diferente de zero. Outros valores acompanharão uma mensagem.
StatusMessage (string): Mensagem de retorno. Em caso de sucesso, esse campo vem com uma
string vazia. Em caso de falhas, uma mensagem com maiores detalhes será retornada.
OrderId (string): Id da ordem de compra no sistema solicitante.
Caso o id do cliente (CustomerId) ou o id da transação vinya (VinyaTransactionId) esteja
incorreto, e o cliente esteja bloqueado ou a requisição esteja incompleta, será retornado um
http status 400 e o objeto descrito acima com o status code diferente de zero.

Caso haja algum problema com a Cancelamento junto ao Adquirente, será retornado um http
status 200, status code diferente de zero e uma mensagem vinda do Adquirente.
Se tudo ocorrer corretamente, será retornado um http status 200, status code zero e nenhuma
status message.
Para realizar testes de Cancelamento na Sandbox Vinya, é indispensável ter os seguintes
dados:
Endpoint da sandbox: https://sandboxec1.vinya.com.br/api/CancelationRequest;
VinyaToken (exemplo: B10DEA11-31D4-4AB7-E0DE-CC3CA1D55A7D);
VinyaTransactionId (exemplo: A7E32614-2884-4E22-86FC-F4690E944719);
CustomerId (exemplo: 150CEFA6-C150-43A4-8A61-0E82F60F0EF0).
Para realização dos testes em sandbox, dois ações anteriores são necessárias:
1) É preciso realizar um teste válido de Autorização e Captura descrito acima e ser
armazenado o VinyaTransactionId para ser passado com o objeto de entrada de
Cancelamento.
Posteriormente, após os testes em sandbox e a homologação da solução, novas credenciais
(VinyaToken e ContractToken) deverão ser geradas para utilização no ambiente de produção.
