module Net::HTTPHeader
  def capitalize(name)
    return "VinyaToken" if name == "vinyatoken"
    name.to_s.split(/-/).map {|s| s.capitalize }.join('-')
  end
  private :capitalize
end
