module Vinya
  class MdmResource < Resource
    self.format = ::JsonFormatter.new(:collection_name)

    def set_attr_resource
      Vinya::MdmResource.set_headers
    end

    def self.set_headers
      self.headers["VinyaToken"] = Vinya.api_token
      self.headers["CustomerId"] = Vinya.customer_id
      self.site = Vinya.api_mdm_endpoint
      self.include_format_in_path = false
      self
    end

    def self.find(id)
      set_headers
      super id
    end
  end
end
