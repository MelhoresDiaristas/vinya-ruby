module Vinya
  class AmountShareData
    attr_accessor :reference, :fields

    def initialize(reference, fields=[])
      @reference = reference
      @fields = fields
    end
  end
end
