module Vinya
  class RetainerPartnerInfo < MdmResource
    self.collection_name = "retainerpartner"

    self.schema = {
      'PartnerId' => :integer,
      'PartnerEmail' => :string,
      'LegalName' => :string,
      'FantasyName' => :string,
      'DocNumber' => :string,
      'BirthDay' => :integer,
      'BirthMonth' => :integer,
      'BirthYear' => :integer,
      'Phone' => :string,
      'Site' => :string,
      'Street' => :string,
      'StreetNumber' => :string,
      'AddressComplement' => :string,
      'District' => :string,
      'City' => :string,
      'State' => :string,
      'ZipCode' => :string,
      'Country' => :string,
      'ResponsibleName' => :string,
      'ResponsibleDocNumber' => :string,
      'ResponsibleDocType' => :integer,
      'ResponsibleEmail' => :string,
      'ResponsiblePhone' => :string,
      'BankName' => :string,
      'BankNumber' => :string,
      'BranchNumber' => :string,
      'BranchCD' => :string,
      'AccountNumber' => :string,
      'AccountCD' => :string,
      'AccountType' => :string
    }

    self.schema.keys.each do |attribute|
      define_method :"#{attribute.underscore}" do
        send(attribute)
      end
      define_method :"#{attribute.underscore}=" do |value|
        send("#{attribute}=", value)
      end
    end

    validates :BirthDay, :BirthMonth, :BirthYear, :PartnerId, :ResponsibleDocType, :presence => true, :numericality => true
    validates :ResponsibleDocType, inclusion: { in: [0, 1, 2], message: "ResponsibleDocType inválida, 0 para CPF, 1 para RG, 2 para passaport" }
    validates :AccountType, inclusion: { in: ["C/C", "poupança"], message: "AccountType inválida, “C/C” = Conta Corrente; “poupança” = Poupança" }

    def id
      self.PartnerId
    end

    def new?
      id.to_i == 0
    end

    def id=(id)
      self.PartnerId = id
    end

    def initialize(attributes = {}, persisted = false)
      super
      self.id = 0 if self.id.blank?
      self
    end

    def self.testObject
      Vinya::RetainerPartnerInfo.new({
        PartnerId: 0,
        PartnerEmail: "feliphepozer@hotmail.com",
        LegalName: "Feliphe Pozzer",
        FantasyName: "Feliphe Pozzer",
        DocNumber: "016.191.770-48",
        BirthDay: 24,
        BirthMonth: 8,
        BirthYear: 1973,
        Phone: "(21) 99706-0629",
        Site: "",
        Street: "Rua Van Erven",
        StreetNumber: "24",
        AddressComplement: "",
        District: "Catumbi",
        City: "Rio de Janeiro",
        State: "RJ",
        ZipCode: "20211-320",
        Country: "Brasil",
        ResponsibleName: "Feliphe Pozzer",
        ResponsibleDocNumber: "016.191.770-48",
        ResponsibleDocType: 1,
        ResponsibleEmail: "feliphepozer@hotmail.com",
        ResponsiblePhone: "(21) 99706-0629",
        BankName: "Caixa",
        BankNumber: "104",
        BranchNumber: "0209",
        BranchCD: "",
        AccountNumber: "014016",
        AccountCD: "1",
        AccountType: "poupança"
      })
    end


  end

end
