
# == Schema Information
#
# Request Params
#
#{
#  "amount": 0,
#  "vinyaTransactionId": "string",
#  "customerId": "string"
#}
#Return Schema
#
# Campos os campos StatusCode, StatusMessage. Serão preenchidos após o post request
#
#{
#  "VinyaTransactionId": "0f35063e-c8bf-4def-a621-d306fed808a5",
#  "StatusCode": 0,
#  "StatusMessage": "",
#  "OrderId": "31152b33-e5d5-4528-ab9c-7d711d41a12a"
#}
## Test CancelationRequest
#cancel = Vinya::CancelationRequest.testObject
#cancel.save
module Vinya
  class CancelPayment < Resource

    self.collection_name = "CancelationRequest"

    self.schema = {
      'amount' => :integer,
      'vinyaTransactionId' => :string,
      'customerId' => :string,
      'StatusCode' => :integer,
      'StatusMessage' => :string

    }

    validates :amount, :presence => true, :length => {:minimum => 0 }, :numericality => true
    validates :vinyaTransactionId, :customerId, :presence => true

    def create
      run_callbacks :create do
        connection.post(collection_path, encode, self.class.headers).tap do |response|
          self.id = id_from_response(response)
          load_attributes_from_response(response)
        end
      end
    end

    def id
      self.VinyaTransactionId
    end

    def code
      self.StatusCode
    end

    def message
      self.StatusMessage
    end

    def self.testObject
      Vinya::CancelationRequest.new({amount: 1000,
        vinyaTransactionId: "2ad8ac35-3831-443f-8d6d-bbd65cdf518e",
        customerId: "DACD7B1F-76B7-4FFF-9E61-E9A0531C7E8E"
      })
    end


  end
end
