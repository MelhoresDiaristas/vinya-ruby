module Vinya
  class AmountShareField
    attr_accessor :id, :name, :value

    def initialize(id, name, value)
      @id = id.to_i
      @name = name
      @value = value.to_i
    end
  end
end
