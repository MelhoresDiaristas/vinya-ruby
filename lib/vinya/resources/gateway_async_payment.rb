# == Schema Information
#
# Request Params
#
#{
#  "amount": 0,
#  "creditCardBrand": "string",
#  "creditCardNumber": "string",
#  "month": 0,
#  "year": 0,
#  "securityCode": "string",
#  "holderName": "string",
#  "installment": 0,
#  "orderId": "string",
#  "customerId": "string",
#  "amountShareData": {
#    "reference": "string",
#    "fields": [
#      {
#        "id": 0,
#        "name": "string",
#        "value": 0
#      }
#    ]
#  }
#}
# Return Schema
#
# Campos os campos VinyaTransactionId, StatusCode, StatusMessage. Serão preenchidos após o post request
#
#{
#  "VinyaTransactionId": "0f35063e-c8bf-4def-a621-d306fed808a5",
#  "StatusCode": 0,
#  "StatusMessage": "",
#  "OrderId": "31152b33-e5d5-4528-ab9c-7d711d41a12a"
#}
#
## Test GatewayPayment
#gtp = Vinya::GatewayAsyncPayment.testObject
#gtp.save
module Vinya
  class GatewayAsyncPayment < GatewayPayment
    self.collection_name = "GatewayAsyncPayment"

    def self.testObject
      Vinya::GatewayAsyncPayment.new({amount: 1000,
        creditCardBrand: "visa",
        creditCardNumber: "0000000000000001",
        month: 10,
        year: 2020,
        securityCode: 123,
        holderName: "João da Silva",
        installment: 1,
        orderId: "Request #45",
        customerId: "DACD7B1F-76B7-4FFF-9E61-E9A0531C7E8E"
      })
    end

    def captureObject
      Vinya::GatewayAsyncPayment.new({
      "Amount": 1000,
      "CustomerId": "DACD7B1F-76B7-4FFF-9E61-E9A0531C7E8E",
      "OrderId": "31152b33-e5d5-4528-ab9c-7d711d41a12a",
      "VinyaTransactionId": "8d8bebb5-202c-4a97-94d2-6a61e3498f57"})
    end
  end
end
