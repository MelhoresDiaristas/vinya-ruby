
# == Schema Information
#
# Request Params
#
#{
#  "amount": 0,
#  "vinyaTransactionId": "string",
#  "customerId": "string",
#  "orderId": "string"
#}
#Return Schema
#
# Campos os campos StatusCode, StatusMessage. Serão preenchidos após o post request
#
# PUT #GatewayAsyncPayment
#{
#  "VinyaTransactionId": "0f35063e-c8bf-4def-a621-d306fed808a5",
#  "StatusCode": 0,
#  "StatusMessage": "",
#  "OrderId": "31152b33-e5d5-4528-ab9c-7d711d41a12a",
#  "antecipatedPayment": true
#}
## Test CancelationRequest
#capture = Vinya::CancelationRequest.testObject
#capture.save
module Vinya
  class CapturePayment < Resource
    self.collection_name = "GatewayAsyncPayment"
    
    self.schema = {
      'amount' => :integer,
      'vinyaTransactionId' => :string,
      'customerId' => :string,
      'orderId' => :integer,
      'antecipatedPayment' => :boolean,
      'sharePartnerId' => :integer
    }

    validates :amount, :presence => true, :length => {:minimum => 0 }, :numericality => true
    validates :vinyaTransactionId, :customerId, :orderId, :antecipatedPayment, :presence => true

    def new?
      false
    end

    def self.testObject
      Vinya::CapturePayment.new({
        amount: 1000,
        customerId: "DACD7B1F-76B7-4FFF-9E61-E9A0531C7E8E",
        orderId: "31152b33-e5d5-4528-ab9c-7d711d41a12a",
        vinyaTransactionId: "8d8bebb5-202c-4a97-94d2-6a61e3498f57",
        antecipatedPayment: true
      })
    end


  end
end
