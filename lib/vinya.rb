require 'activeresource'
require 'vinya/version'
require 'vinya/connection'
require 'vinya/http_header'
require 'vinya/resource'
require 'vinya/json_formatter'
require 'vinya/resources/gateway_payment'
require 'vinya/mdm_resource'

Dir[File.expand_path('../vinya/resources/*.rb', __FILE__)].map do |path|
  require path
end

module Vinya
  class << self
    attr_accessor :api_endpoint, :api_token, :api_mdm_endpoint, :customer_id
  end

  self.api_endpoint     = ENV['VINYA_API_ENDPOINT'] || 'https://sandboxec1.vinya.com.br/api'
  self.api_token        = ENV['VINYA_TOKEN']
  self.customer_id      = ENV['VINYA_COSTUMER_ID']
  self.api_mdm_endpoint = ENV['VINYA_API_MDM_ENDPOINT'] || 'https://mdm-sandbox.vinya.com.br/api'
end
